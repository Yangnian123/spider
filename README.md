# SPIDER
## 这是一个爬虫软件
1.maven 构建
2.依赖 WebCollector
3.jdk必须1.8以上哦

运行ImageCrawler类中的main方法，将会抓取一个图片网站的高清图片，图片将保存到本地路径D:\spider\picture中。


----------
其实我已经将这些代码打包成了一个jar文件
[点击我下载jar包](https://git.oschina.net/OrangeLeague/spider/attach_files/download?i=75239&u=http%3A%2F%2Ffiles.git.oschina.net%2Fgroup1%2FM00%2F00%2FF3%2FPaAvDFi70oKABRO-AFLrsmDe6Hc350.jar%3Ftoken%3D8b0594d18bdebe61c650548da3f2c716%26ts%3D1488704130%26attname%3Dspider-1.0-SNAPSHOT.jar)

在cmd窗口中进入jar包所在目录，执行命令**java -jar spider-1.0-SNAPSHOT.jar**
你就会看到大量高清美图源源不断的下载到你的D:\spider\picture文件夹下。
你可以让程序跑个几天几夜，但是请准备好足够的硬盘空间。

----------

如果觉得这个程序不错，请FORK一份，或者点个赞！好东西要分享，记住我，庸俗而不低俗，入流而不下流。
**注意：文件夹中文件数量超过一万，索引会变慢。但不要担心，有时间我会改良一下程序，自动分组新建文件夹。**
![输入图片说明](http://git.oschina.net/uploads/images/2017/0305/165146_46b75189_339062.jpeg "在这里输入图片标题")